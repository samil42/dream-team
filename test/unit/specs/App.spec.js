import Vue from 'vue';
import {store} from '../../../src/store';
import actions from '../../../src/store/actionTypes';
import App from '../../../src/App.vue';

describe('App', () => {
    it('has a created hook', () => {
        expect(typeof App.created).to.be.equal('function')
    });

    //TODO: [Vue warn]: Unknown custom element: <router-view>
    xit('should dispatch action on create', done => {
        let spy = sinon.spy(store, 'dispatch');

        const vm = new Vue({
            template: '<div><test></test></div>',
            store,
            components: {
                'test': App
            }
        }).$mount();


        Vue.nextTick()
            .then(() => {
                spy.calledWith(actions.APP_CREATED).should.be.true;
                spy.restore();
                done()
            })
            .catch(done)

    })
});