import TrackItem from '../../../../src/components/TrackItem';

describe('TrackItem', () => {
    it('should format duration correctly', () => {
        expect(TrackItem.methods.formatDuration(200000)).to.be.equal('(3:20)');
        expect(TrackItem.methods.formatDuration(188765)).to.be.equal('(3:08)');
    })
});