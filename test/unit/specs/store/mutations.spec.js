import { expect } from 'chai';
import mutations from '../../../../src/store/mutations';

describe('Mutations', () => {
    it('Login', () => {
        const state = { isLoggedIn: false };
        mutations.LOGIN(state);
        expect(state.isLoggedIn).to.be.true;
    });

    it('Logout', () => {
        const state = { isLoggedIn: true };
        mutations.LOGOUT(state);
        expect(state.isLoggedIn).to.be.false;
    });

    it('Start loading', () => {
        const state = { isLoading: false };
        mutations.START_LOADING(state);
        expect(state.isLoading).to.be.true;
    });

    it('Stop loading', () => {
        const state = { isLoading: true };
        mutations.STOP_LOADING(state);
        expect(state.isLoading).to.be.false;
    });

    it('Set user data', () => {
        const state = { userData: {} };
        mutations.SET_USER_DATA(state, {id: 1, name: 'Sanya'});
        expect(state.userData).to.be.deep.equal({id: 1, name: 'Sanya'});
    });

    it('Set user playlists', () => {
        const state = { userData: {someData: 'some string'} };
        mutations.SET_USER_PLAYLISTS(state, [1, 2]);
        expect(state.userData).to.be.deep.equal({someData: 'some string', playlists: [1, 2]});
    });

    it('Set current playlist', () => {
        const state = {};
        mutations.SET_CURRENT_PLAYLIST(state, {id: 'playlist id'});
        expect(state).to.be.deep.equal({currentPlaylist: {id: 'playlist id'}});
    });

    it('Set current playlist', () => {
        const state = {};
        mutations.SET_CURRENT_PLAYLIST(state, {id: 'playlist id'});
        expect(state).to.be.deep.equal({currentPlaylist: {id: 'playlist id'}});
    });

    it('Remove track from current playlist', () => {
        const state = {currentPlaylist: {tracks: {items: [
            {track: {uri: '1'}},
            {track: {uri: '2'}},
            {track: {uri: '3'}},
            {track: {uri: '4'}},
        ]}}};

        mutations.TRACK_REMOVED(state, '3');

        expect(state.currentPlaylist).to.be.deep.equal({tracks: {items: [
            {track: {uri: '1'}},
            {track: {uri: '2'}},
            {track: {uri: '4'}},
        ]}});
    });

    it('Set search results', () => {
        const state = {};
        mutations.SET_SEARCH_RESULTS(state, ['item1', 'items2']);
        expect(state).to.be.deep.equal({searchResults: ['item1', 'items2']});
        mutations.SET_SEARCH_RESULTS(state, null);
        expect(state).to.be.deep.equal({searchResults: null});
    });

    it('Add track to current playlist', () => {
        const state = {
            searchResults: [
                {uri: '1'},
                {uri: '2'},
                {uri: '3'}
            ],
            currentPlaylist: {
                tracks: {
                    items: [
                        {track: {uri:'10'}},
                        {track: {uri:'11'}},
                        {track: {uri:'12'}}
                    ]
                }
            }
        };

        mutations.TRACK_ADDED(state, '2');
        expect(state).to.be.deep.equal({
            searchResults: [
                {uri: '1'},
                {uri: '3'}
            ],
            currentPlaylist: {
                tracks: {
                    items: [
                        {track: {uri:'10'}},
                        {track: {uri:'11'}},
                        {track: {uri:'12'}},
                        {track: {uri:'2'}}
                    ]
                }
            }
        });
    });
});
