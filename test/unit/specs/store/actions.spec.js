import { expect } from 'chai';
import * as actions from '../../../../src/store/actions';

const testAction = (action, payload, state, expectedMutations, done) => {
    let count = 0;

    const commit = (type, payload) => {
        const mutation = expectedMutations[count];

        try {
            expect(mutation.type).to.equal(type);
            payload && expect(mutation.payload).to.deep.equal(payload)
        } catch (error) {
            done(error);
        }

        count++;
        if (count >= expectedMutations.length) {
            done()
        }
    };

    action({ commit, state }, payload);

    if (expectedMutations.length === 0) {
        expect(count).to.equal(0);
        done();
    }
};

describe('actions', () => {
    it('logout', done => {
        testAction(actions.LOGOUT, null, {}, [
            {type: 'LOGOUT' },
            {type: 'STOP_LOADING'}
        ], done)
    });

    //TODO: Write more tests, using sinon.stub or actionsInjector or sinon.fakeServer for stubbing server calls
});
