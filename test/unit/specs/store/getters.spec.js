import { expect } from 'chai';
import getters from '../../../../src/store/getters';

describe('Getters', () => {
    it('Header text', () => {
        let state = {
            headerText: 'Hi',
            isLoggedIn: false,
            userData: {}
        };

        expect(getters.headerText(state)).to.equal('Hi, guest!');

        state.isLoggedIn = true;

        expect(getters.headerText(state)).to.equal('Hi, user!');

        state.userData.display_name = 'Sanya';

        expect(getters.headerText(state)).to.equal('Hi, Sanya!');
    });
});
