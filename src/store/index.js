import Vue from 'vue';
import Vuex from 'vuex';

import * as actions from './actions'
import getters from './getters'
import mutations from './mutations'

Vue.use(Vuex);

const state = {
    headerText: 'Hi',
    isLoggedIn: !!localStorage.getItem('spotify_token'),
    isLoading: false,
    userData: {},
    currentPlaylist: null,
    searchResults: null
};

export const store = new Vuex.Store({
    strict: true,
    state,
    mutations,
    actions,
    getters
});
