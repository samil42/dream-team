import mutations from './mutationTypes';
import * as api from '../api';

const AUTH_TOKEN = 'spotify_token';

export const LOGIN = () => {
    api.authentication(); //redirect to Spotify page
};

export const LOGOUT = ({commit}) => {
    localStorage.removeItem(AUTH_TOKEN);
    commit(mutations.LOGOUT);
    commit(mutations.STOP_LOADING);
};

/**
 * Obtains parameters from the hash of the URL
 * @return Object
 */
const _getHashParams = () => {
    let hashParams = {};
    let e, r = /([^&;=]+)=?([^&;]*)/g,
        q = window.location.hash.substring(1);

    while ( e = r.exec(q)) {
        hashParams[e[1]] = decodeURIComponent(e[2]);
    }

    return hashParams;
};

export const APP_CREATED = ({commit, state}) => {
    const params = _getHashParams();
    const accessToken = params.access_token;

    const token = accessToken || localStorage.getItem(AUTH_TOKEN);
    if (token) {
        commit(mutations.START_LOADING);

        api.checkAuthentication(token)
            .then((response) => {
                localStorage.setItem(AUTH_TOKEN, token);
                commit(mutations.LOGIN);
                commit(mutations.SET_USER_DATA, response.data);

                api.getListOfUsersPlaylists(state.userData.id, token)
                    .then((response) => commit(mutations.SET_USER_PLAYLISTS, response.data.items))
                    .catch((error) => console.log(error))
                    .then(() => commit(mutations.STOP_LOADING))
            })
            .catch((error) => LOGOUT({commit}))
    } else {
        LOGOUT({commit});
    }
};

export const SHOW_DETAILED_PLAYLIST = ({commit, state}, id) => {
    commit(mutations.START_LOADING);

    api.getPlaylist(state.userData.id, localStorage.getItem(AUTH_TOKEN), id)
        .then((response) => commit(mutations.SET_CURRENT_PLAYLIST, response.data))
        .catch((error) => console.log(error))
        .then(() => commit(mutations.STOP_LOADING))
};

export const REMOVE_TRACK = ({commit, state}, uri) => {
    commit(mutations.START_LOADING);

    api.removeTrack(state.userData.id, localStorage.getItem(AUTH_TOKEN), state.currentPlaylist.id, uri)
        .then(() => commit(mutations.TRACK_REMOVED, uri))
        .catch((error) => console.log(error))
        .then(() => commit(mutations.STOP_LOADING))
};

export const SEARCH_TRACK = ({commit}, query) => {
    commit(mutations.START_LOADING);

    api.searchTrack(localStorage.getItem(AUTH_TOKEN), query)
        .then((response) => commit(mutations.SET_SEARCH_RESULTS, response.data.tracks.items))
        .catch((error) => console.log(error))
        .then(() => commit(mutations.STOP_LOADING))
};

export const ADD_TRACK_TO_PLAYLIST = ({commit, state}, uri) => {
    api.addTrack(state.userData.id, localStorage.getItem(AUTH_TOKEN), state.currentPlaylist.id, uri)
        .then(() => commit(mutations.TRACK_ADDED, uri))
        .catch((error) => console.log(error))
        .then(() => commit(mutations.STOP_LOADING))
};

export const NEW_TRACK_CONTAINER_CREATED = ({commit}) => {
    commit(mutations.SET_SEARCH_RESULTS, null);
};
