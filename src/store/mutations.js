export default {
    LOGIN: (state) => {
        state.isLoggedIn = true;
    },

    LOGOUT: (state) => {
        state.isLoggedIn = false;
    },

    START_LOADING: (state) => {
        state.isLoading = true;
    },

    STOP_LOADING: (state) => {
        state.isLoading = false
    },

    SET_USER_DATA: (state, payload) => {
        state.userData = payload;
    },

    SET_USER_PLAYLISTS: (state, payload) => {
        state.userData = { ...state.userData, playlists: payload};
    },

    SET_CURRENT_PLAYLIST: (state, payload) => {
        state.currentPlaylist = payload;
    },

    TRACK_REMOVED: (state, payload) => {
        state.currentPlaylist.tracks.items = state.currentPlaylist.tracks.items.filter(
            item => item.track.uri !== payload
        );
    },

    SET_SEARCH_RESULTS: (state, payload) => {
        state.searchResults = payload;
    },

    TRACK_ADDED: (state, payload) => {
        const index = state.searchResults.findIndex(item => item.uri === payload);
        const track = state.searchResults.splice(index, 1)[0];

        state.currentPlaylist.tracks.items = [...state.currentPlaylist.tracks.items, {track}];
    }
}
