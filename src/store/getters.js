export default {
    headerText: state => {
        let text = state.headerText;

        text += ', ';

        if (state.isLoggedIn) {
            let name = state.userData.display_name;
            text += name ? name : 'user';
        } else {
            text += 'guest';
        }

        text += '!';

        return text;
    }
}
