import axios from 'axios';

const clientId = '0c1e37be70af4ecfaf23b7ac3959892b';
const base = 'https://api.spotify.com/v1/';
// const clientSecret = 'bdc52eaa00714c73b2c310e378b959af';

export const authentication = () => {
    const scopes = 'playlist-read-private playlist-modify-private playlist-modify-public';
    const redirectUrl = 'http://localhost:8888/';

    let url = 'https://accounts.spotify.com/authorize';
    url += '?response_type=token';
    url += '&client_id=' + encodeURIComponent(clientId);
    url += '&scope=' + encodeURIComponent(scopes);
    url += '&redirect_uri=' + encodeURIComponent(redirectUrl);

    window.location = url;
};

export const checkAuthentication = (token) => {
    return axios.get(base + 'me', {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    })
};

export const getListOfUsersPlaylists = (userId, token) => {
    return axios.get(base + 'users/' + userId + '/playlists', {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    })
};

export const getPlaylist = (userId, token, playlistId) => {
    return axios.get(base + 'users/' + userId + '/playlists/' + playlistId, {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    })
};

export const removeTrack = (userId, token, playlistId, uri) => {
    return axios.delete(base + 'users/' + userId + '/playlists/' + playlistId + '/tracks', {
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
        data: { 'tracks': [{ 'uri': uri }] }
    })
};

export const searchTrack = (token, query) => {
    return axios.get(base + 'search?q=' + encodeURIComponent(query) + '&type=track', {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    })
};

export const addTrack = (userId, token, playlistId, uri) => {
    return axios.post(base + 'users/' + userId + '/playlists/' + playlistId + '/tracks?uris=' + encodeURIComponent(uri), {}, {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    })
};