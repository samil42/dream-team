import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import {store} from './store';
import Home from './components/Home';
import PlaylistDetailed from './components/PlaylistDetailed';
Vue.config.productionTip = false;

Vue.use(VueRouter);

const routes = [
    {path: '/', component: Home},
    {path: '/playlist/:playlistId', component: PlaylistDetailed},
    {path: '*', redirect: '/'}
];

const router = new VueRouter({
    routes,
    mode: 'history'
});

new Vue({
    el: '#app',
    store,
    router,
    render: h => h(App)
});
